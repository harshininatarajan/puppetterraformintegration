# PuppetTerraformIntegration

## Overview

This repository demonstrates the automation of configuration management using Puppet for infrastructure provisioned with Terraform. The integration between these two tools enables the seamless provisioning and configuration of infrastructure resources.

## Requirements

1. **Setup GitLab Repository:**
   - Create a new GitLab repository named "PuppetTerraformIntegration".
   - Initialize the repository with a simple README.md file.

2. **Infrastructure Provisioning with Terraform:**
   - Write Terraform code to provision a simple infrastructure component (e.g., AWS EC2 instance, Azure VM, etc.).
   - Store the Terraform configuration files in a directory named "terraform".

3. **Configuration Management with Puppet:**
   - Write Puppet manifests to configure the provisioned infrastructure component.
   - Puppet tasks should include installing required packages or software, configuring system settings or files, and starting or enabling services.
   - Store the Puppet manifests and modules in a directory named "puppet".

4. **Integration:**
   - Implement a mechanism to automatically trigger Puppet configuration management after Terraform provisioning.
   - Options include using Terraform provisioners to trigger Puppet runs on provisioned instances or utilizing configuration management tools like Ansible or Shell scripts within Terraform to execute Puppet runs remotely.


5. **Testing:**
   - Test the integration by provisioning infrastructure using Terraform and ensuring that Puppet successfully configures the provisioned resources.
   - Validate that the provisioned resources meet the desired configuration state defined in Puppet manifests.
